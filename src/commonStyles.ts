import {StyleSheet, ViewStyle} from 'react-native';

export const Colors = {
    lightPink: '#f3c6d0',
    darkPink: '#f39292',
    yellow: '#d0aa66',
    purple: '#9d88d0',
    blue: '#418ccd',
};

export const commonStyles = StyleSheet.create<{
    container: ViewStyle,
    bezel: ViewStyle,
}>({
    container: {
        flex: 1,
    },
    bezel: {
        margin: 'auto',
        minWidth: 165,
        padding: 8,
        borderRadius: 10,
        borderWidth: 2,
        backgroundColor: 'grey',
    },
});

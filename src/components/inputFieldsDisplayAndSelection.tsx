import React from 'react';
import {Text, TouchableOpacity, View, StyleSheet, TextStyle, ViewStyle} from 'react-native';
import {inputField} from '../screens/paymentScreen';
import {commonStyles, Colors} from '../commonStyles';
import _ from 'lodash';

export default function InputFieldsDisplayAndSelection(props: {
    cardNumber: string,
    expiration: string,
    cvcNumber: string,
    selectedField: inputField,
    onSelectField: (input: number | string) => void,
}): React.ReactElement<View> {
    const FieldArea = (field: inputField): React.ReactElement<TouchableOpacity> => {
        const isSelected = (props.selectedField === field);
        return (
            <TouchableOpacity
                style={[styles.fieldSelection, isSelected && {backgroundColor: Colors.darkPink}]}
                onPress={() => props.onSelectField(field)}
                key={field}
                disabled={isSelected}
            >
                <Text>{field}</Text>
                <Text style={styles.fieldTextDisplay}>{props[field]}</Text>
            </TouchableOpacity>
        );
    };
    return <View style={commonStyles.bezel}>
        {
            _.map(inputField, ((field: inputField) => FieldArea(field)))
        }
    </View>
}

const styles = StyleSheet.create<{
    fieldSelection: ViewStyle,
    fieldTextDisplay: TextStyle,
}>({
    fieldSelection: {
        borderRadius: 8,
        padding: 12,
        margin: 2,
        flex: 1,
        backgroundColor: Colors.lightPink,
    },
    fieldTextDisplay: {
        backgroundColor: Colors.blue,
        borderWidth: 0.5,
        minHeight: 18,
        textAlign: 'right',
        paddingHorizontal: 4,
    }
});

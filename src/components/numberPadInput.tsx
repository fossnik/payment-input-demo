import React from 'react';
import {TouchableOpacity, Text, View, StyleSheet, ViewStyle} from 'react-native';
import {commonStyles, Colors} from '../commonStyles';
import {inputField} from '../screens/paymentScreen';

export default function NumberPadInput(props: {
    cardNumber: string,
    expiration: string,
    cvcNumber: string,
    selectedField: inputField,
    onUserInput: (input: number | string) => void,
}): React.ReactElement<View> {
    const NumberButton = (text: number | string): React.ReactElement<TouchableOpacity> => (
        <TouchableOpacity
            style={[styles.numButton, text === 'clear' && {backgroundColor: Colors.darkPink}]}
            onPress={() => props.onUserInput(text)}
            key={text}
        >
            <Text>{String(text)}</Text>
        </TouchableOpacity>
    );

    return <View style={commonStyles.bezel}>
        <View style={styles.inputDisplay}>
            <Text>{props[props.selectedField]}</Text>
        </View>
        {
            [
                [7, 8, 9],
                [4, 5, 6],
                [1, 2, 3],
                [0, 'clear', 'del']
            ].map((row: [number | string]) => (
                <View style={styles.numRow}>
                    {row.map(n => NumberButton(n))}
                </View>
            ))
        }
    </View>
}

const styles = StyleSheet.create<{
    inputDisplay: ViewStyle,
    numRow: ViewStyle,
    numButton: ViewStyle,
}>({
    inputDisplay: {
        backgroundColor: Colors.blue,
        marginVertical: 4,
        borderRadius: 2,
        borderWidth: 1,
        minHeight: 30,
        minWidth: 130,
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    numRow: {
        flexDirection: 'row',
    },
    numButton: {
        flex: 1,
        padding: 10,
        margin: 2,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        backgroundColor: Colors.lightPink,
    },
});

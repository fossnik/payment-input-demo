import React, {Component} from 'react';
import {SafeAreaView, View} from 'react-native';
import InputFieldsDisplayAndSelection from '../components/inputFieldsDisplayAndSelection';
import NumberPadInput from '../components/numberPadInput';
import {commonStyles, Colors} from '../commonStyles';

export enum inputField {
    cardNumber = 'cardNumber',
    expiration = 'expiration',
    cvcNumber = 'cvcNumber',
}

class PaymentScreen extends Component<{}, {
    cardNumber?: string,
    expiration?: string,
    cvcNumber?: string,
    selectedField?: inputField,
}> {
    constructor(props) {
        super(props);

        this.state = {
            cardNumber: '',
            expiration: '',
            cvcNumber: '',
            selectedField: inputField.cardNumber,
        };

        this.onUserInput = this.onUserInput.bind(this);
        this.onSelectField = this.onSelectField.bind(this);
    }

    onUserInput(input: number | string): void {
        const selectedField: inputField = this.state.selectedField;

        if (input === 'del') {
            this.setState({
                [selectedField]: this.state[selectedField].slice(0, -1),
            })
        } else if (input === 'clear') {
            this.setState({
                [selectedField]: '',
            })
        } else {
            this.setState({
                [selectedField]: `${this.state[selectedField]}${input}`,
            })
        }
    }

    onSelectField(input: inputField): void {
        this.setState({
            selectedField: input,
        })
    }

    render(): React.ReactElement<SafeAreaView> {
        return <SafeAreaView style={{
            flex: 1,
            flexDirection: 'row',
        }}>
            <View style={[commonStyles.container, {
                backgroundColor: Colors.yellow,
            }]}>
                <InputFieldsDisplayAndSelection
                    cardNumber={this.state.cardNumber}
                    expiration={this.state.expiration}
                    cvcNumber={this.state.cvcNumber}
                    selectedField={this.state.selectedField}
                    onSelectField={this.onSelectField}
                />
            </View>
            <View style={[commonStyles.container, {
                backgroundColor: Colors.purple,
            }]}>
                <NumberPadInput
                    cardNumber={this.state.cardNumber}
                    expiration={this.state.expiration}
                    cvcNumber={this.state.cvcNumber}
                    selectedField={this.state.selectedField}
                    onUserInput={this.onUserInput}
                />
            </View>
        </SafeAreaView>
    }
}

export default PaymentScreen;
